public class Employee {

  private String name;
  private int hourWorked;
  private double hourlyRate;
  private String position;

  public Employee(
    String nameValue,
    int hourWorkedValue,
    double hourlyRateValue,
    String positionValue
  ) {
    this.name = nameValue;
    this.hourWorked = hourWorkedValue;
    this.hourlyRate = hourlyRateValue;
    this.position = positionValue;
  }

  public String getName() {
    return this.name;
  }

  public int getHourWorked() {
    return this.hourWorked;
  }

  public double getHourlyRate() {
    return this.hourlyRate;
  }

  public String getPosition() {
    return this.position;
  }
}
