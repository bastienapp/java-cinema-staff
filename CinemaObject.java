class CinemaObject {

  public static void main(String[] args) {


    /*String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
    int[] hoursWorked = { 35, 38, 35, 38, 40 };
    double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
    String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };*/

    Employee alice = new Employee("Alice Dupont", 35, 12.5, "Caissier");
    Employee bob = new Employee("Bob Martin", 38, 15.0, "Projectionniste");

    Employee[] employees = { alice, bob };

    for (int i = 0; i < employees.length; i++) {
      // récupérer l'employé en cours dans la boucle
      Employee currentEmployee = employees[i];

      double salary;
      int overtime = currentEmployee.getHourWorked() - 35;
      if (overtime > 0) {
        salary = (35 * currentEmployee.getHourlyRate()) + (overtime * (currentEmployee.getHourWorked() * 1.5));
      } else {
        salary = currentEmployee.getHourWorked() * currentEmployee.getHourlyRate();
      }
      System.out.println(currentEmployee.getName() + " (" + currentEmployee.getPosition() + ") : " + salary);
    }
    /*
    String searchPosition = "Tacos";
    var count = 0;
    for (int i = 0; i < employeeNames.length; i++) {
      String currentEmployee = employeeNames[i];
      String currentJob = positions[i];
      // equals : objets et String
      if (searchPosition.equals(currentJob)) {
        System.out.println(currentEmployee);
        count++;
      }
    }
    // == : pour les types primitifs
    if (count == 0) {
      System.out.println("Aucun employé trouvé.");
    } */
  }
}