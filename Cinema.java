class Cinema {

  public static void main(String[] args) {

    String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
    int[] hoursWorked = { 35, 38, 35, 38, 40 };
    double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
    String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };

    // i 0 1 2 3 4 5
    // [35, 38, 35, 38, 40] x
    // length : 5
    for (int i = 0; i < employeeNames.length; i++) {
      String currentEmployee = employeeNames[i];
      int currentHour = hoursWorked[i];
      double currentRate = hourlyRates[i];
      String currentJob = positions[i];

      double salary;
      int overtime = currentHour - 35;
      if (overtime > 0) {
        salary = (35 * currentRate) + (overtime * (currentHour * 1.5));
      } else {
        salary = currentHour * currentRate;
      }
      // System.out.println(currentEmployee + " (" + currentJob + ") : " + salary);
    }

    String searchPosition = "Tacos";
    var count = 0;
    for (int i = 0; i < employeeNames.length; i++) {
      String currentEmployee = employeeNames[i];
      String currentJob = positions[i];
      // equals : objets et String
      if (searchPosition.equals(currentJob)) {
        System.out.println(currentEmployee);
        count++;
      }
    }
    // == : pour les types primitifs
    if (count == 0) {
      System.out.println("Aucun employé trouvé.");
    }
  }
}